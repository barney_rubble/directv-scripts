# Fred's scripts

How to get:

    git clone ssh://git@egbitbucket.dtvops.net:7999/~fl134e/scripts.git fred-scripts

Or you can try this, but it doesn't seem to work for me:

    git clone https://egbitbucket.dtvops.net/scm/~fl134e/scripts.git fred-scripts

## Bug Reports

### [bugreport.fix](https://egbitbucket.dtvops.net/users/fl134e/repos/scripts/browse/bugreport.fix)

Collates and sorts the different time-stamped sections in bugreport.txt
into one tab-delimited file called "bugreport.txt.sorted" that can be
imported into Excel.

Output format:

    SECTION-NAME  <tab>  TIMESTAMP  <tab>  MESSAGE

Example: Search for Wi-Fi messages

    egrep -i "wifi|wpa|wlan0" bugreport.txt.sorted

### [bugreport.get_provisioning_failures](https://egbitbucket.dtvops.net/users/fl134e/repos/scripts/browse/bugreport.get_provisioning_failures)

Tries to find provisioning failures in bugreport.txt.

### [bugreport.packets-to-tcpdump](https://egbitbucket.dtvops.net/users/fl134e/repos/scripts/browse/bugreport.packets-to-tcpdump)

Extracts packets from three sections in bugreport.txt and creates the
following output files:

    tcpdump-logcat.txt
    tcpdump-eth0.txt
    tcpdump-wlan0.txt

## Git Projects

### [git.rebase_patches](https://egbitbucket.dtvops.net/users/fl134e/repos/scripts/browse/git.rebase_patches)

Usage: git.rebase_patches PATH BASE REMOTE/TARGET-BRANCH COMMITS

Rebases the COMMITS onto BASE and pushes them to REMOTE/TARGET-BRANCH.

## Repo Projects

You must be in the top directory of your AOSP source tree to run these scripts.

### [repo.manifest_get](https://egbitbucket.dtvops.net/users/fl134e/repos/scripts/browse/repo.manifest_get)

Usage: repo.manifest_get COMMIT

Outputs the unified manifest for the specified commit. Actions taken:

    cd .repo/manifests
    git checkout COMMIT
    repo manifest
    git checkout -


### [repo.manifest_list](https://egbitbucket.dtvops.net/users/fl134e/repos/scripts/browse/repo.manifest_list)

Usage: repo.manifest_list COMMIT

Temporary checks out the COMMIT in .repo/manifests and runs "repo list". Actions taken:

    cd .repo/manifests
    git checkout COMMIT
    repo list
    git checkout -

### [repo.manifest_show_commits](https://egbitbucket.dtvops.net/users/fl134e/repos/scripts/browse/repo.manifest_show_commits

Usage: repo.manifest_show_commits COMMIT

Temporary checks out the COMMIT in .repo/manifests and outputs the manifest in this format:

    PATH <tab> REMOTE <space> REVISION

### [repo.manifest_compare](https://egbitbucket.dtvops.net/users/fl134e/repos/scripts/browse/repo.manifest_compare)

Usage: repo.manifest_compare COMMIT1 COMMIT2

Temporary checks out the manifest commits outputs the manifest differences in this format:

    PATH <tab> REMOTE1 <space> URL1 <space> REVISION1 <tab> REMOTE2 <space> URL2 <space> REVISION2

### [repo.diff](https://egbitbucket.dtvops.net/users/fl134e/repos/scripts/browse/repo.diff)

Usage: repo.diff COMMIT1 COMMIT2 [git-diff options]

Shows the project differences between two repo manifest commits.

For example, `repo.diff q-wnc-28.5-2h1.1 q-wnc-28.5-2h1.1-scandebug` shows:

    Project differences between q-wnc-28.5-2h1.1 and q-wnc-28.5-2h1.1-scandebug:

    ========== vendor/broadcom/refsw: 3b25648f5dc24c762faa7e58602065ba1c354d63 vs ac9aed3b68cadbc4f35ec48dea9d5fcbb8f14ae8 ==========

	cd vendor/broadcom/refsw && git log --graph --oneline ac9aed3b68cadbc4f35ec48dea9d5fcbb8f14ae8 ^3b25648f5dc24c762faa7e58602065ba1c354d63^1

	    * ac9aed3b68 Apply STB7271_REL_15_30_71.scanDebug.patch
	    * 3b25648f5d Applied OVOSP-62299 log_disassoc_deauth_corrected.patch

	cd vendor/broadcom/refsw && git diff --stat 3b25648f5dc24c762faa7e58602065ba1c354d63 ac9aed3b68cadbc4f35ec48dea9d5fcbb8f14ae8:

	     BSEAV/connectivity/wlan/7271/src/wl/sys/wl_cfg80211.c | 10 +++++-----
	     BSEAV/connectivity/wlan/7271/src/wl/sys/wlc.c         |  2 +-
	     BSEAV/connectivity/wlan/7271/src/wl/sys/wlc_assoc.c   |  2 +-
	     BSEAV/connectivity/wlan/7271/src/wl/sys/wlc_scan.c    | 15 ++++++++-------
	     4 files changed, 15 insertions(+), 14 deletions(-)

### [repo.rebase_patches](https://egbitbucket.dtvops.net/users/fl134e/repos/scripts/browse/repo.rebase_patches)

Usage: repo.rebase_patches M1 M2 ONTO NEW-BRANCH

Rebases the patches that happened between manifest commits M1 and M2
onto manifest commit ONTO and names the new project and manifest branches
NEW-BRANCH.

For example, to rebase the patches between release-q-wnc-28.5-2 and
q-wnc-28.5-2h1.1noALog onto release-q-wnc-29.2-6 and call the new branch
q-wnc-29.2-6-2h1.1noALog we would run the following command:

    repo.rebase_patches release-q-wnc-28.5-2 q-wnc-28.5-2h1.1noALog release-q-wnc-29.2-6 q-wnc-29.2-6-2h1.1noALog

Note that if there are merge conflicts the script will open up a bash
shell so that the user can fix the conflicts before continuing.
