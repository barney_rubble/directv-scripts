#!/bin/bash

set -o pipefail

usage() {
    echo -e "\nUsage: $0 [-n] PATH BASE REPO/TARGET-BRANCH [COMMITS]

Rebases the COMMITS onto BASE and pushes them to TARGET-BRANCH in REPO.

Options:
    -n      Test actions but do not push changes to REPO/TARGET-BRANCH
    -v      Show actions even when successful

Note: The COMMITS are optional. If you omit COMMITS then you will just
be making a copy of the BASE reference.

"
    exit 1
}

while [[ "$1" == -* ]]; do
    if [ "$1" = "-n" ]; then
        test=1
        shift
    elif [ "$1" = "-v" ]; then
        verbose=1
        shift
    else
        echo "Bad option $1" 1>&2
        exit 1
    fi
done

if [ $# -lt 3 ]; then
    usage
fi

has() {
    git merge-base --is-ancestor "$1" "$2"
}

get_commit() {
    git rev-parse "$1"
}

same_commit() {
    [ "`get_commit $1`" = "`get_commit $2`" ]
}

dir="$1" ; shift
master="$1" ; shift
repo="$1" ; shift

if [[ "$repo" != mc/* ]]; then
    echo Expected $repo to start with mc/
    exit 1
else
    
    target=${repo##mc/}
    repo=mc
fi

save=save.$$
tmp=/tmp/tmp.$$
log=/tmp/log.$$
(
    set -vxe
    cd "$dir" && git fetch --all -p >& /dev/null || exit 1
    (git cherry-pick --abort || true) >& /dev/null
    (git rebase --abort || true) >& /dev/null
    git checkout -b $save
    git stash
    git checkout -f $master
    if [ "$*" ]; then
        if ! git cherry-pick "$@"; then
            cat $log > /dev/tty
            echo -e "\nLaunching shell. Please exit shell when finished.\n" > /dev/tty
            bash -i < /dev/tty >& /dev/tty || true
        fi
    fi

    if [ ! "$test" ]; then
        # Delete previous branch if it exists
        git push $repo :$target || true # ignore errors
        # Push new branch
        git push $repo HEAD:refs/heads/$target
        git show --stat --oneline HEAD $repo/$target
        if ! same_commit HEAD $repo/$target; then
            echo "HEAD `get_commit HEAD` is not the same as $repo/$target `get_commit $repo/$target`" 1>&2
            exit 1
        fi
    fi
    [ "$test" ] && exit 200 || exit 100
    exit 123
) >& $log
status=$?

if [ "$status" = 100 ]; then
    [ "$verbose" ] && cat /tmp/log.$$
    echo $dir was rebased and pushed to $repo/$target
    status=0
elif [ "$status" = 200 ]; then
    [ "$verbose" ] && cat /tmp/log.$$
    echo $dir test rebase was successful
    status=0
else
    echo "Error rebasing commits:"
    cat /tmp/log.$$
    rm -f /tmp/log.$$
fi
# Cleanup
(
    set -xe
    cd "$dir"
    git checkout -f $save --detach
    git stash pop || true
    git branch -D $save || true
) >& /tmp/log.$$
cleanup=$?
if [ "$cleanup" != 0 ]; then
    echo "Error restoring state:"
    cat /tmp/log.$$
    rm -f /tmp/log.$$
    status=$cleanup
fi
exit $status
