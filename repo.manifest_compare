#!/usr/bin/perl

if ($ARGV[0] eq '-a') {
    $show_all = 1;
    shift;
}

@ARGV >= 2 || die "\nUsage: $0 [-a] ref1 ref2 [...]

Options:
    -a      Show all, not just differences

";

my @hashes;
for my $ref (@ARGV) {
    push(@hashes, parse_xml($ref));
}

my @paths = get_paths(@hashes);
print join("\t", "path", @ARGV), "\n";
for my $path (@paths) {
    my @line = ($path);
    for my $hash (@hashes) {
        push(@line, $hash->{$path});
    }
    next if (!$show_all && $line[1] eq $line[2]);
    print join("\t", @line), "\n";
}
exit;

sub run {
    warn "Running: $_[0]\n" if $debug;
    my $tmp = "/tmp/tmp.$$";
    if (system("($_[0]) >& $tmp") != 0) {
        warn "Failed to run command: $_[0]\n";
        system("cat $tmp 1>&2; rm -f $tmp");
        exit 1;
    }
    unlink($tmp);
    return 0;
}

sub parse_xml {
    my ($ref) = @_;
    my $hash;
    run("cd .repo/manifests && git checkout $ref");
    my $default_remote;
    open(FILE, "repo manifest | xml2 2> /dev/null |") || die "Error: $?";
    my ($revision, $path, $name, $remote);
    my ($remote_name, $remote_url);
    my %remotes;
    while (<FILE>) {
        chomp;
        warn "Checking line $_\n" if $debug;
        if (m|^/manifest/remote/\@name=(.*)$|) {
            $remote_name = $1;
        } elsif (m|^/manifest/remote/\@fetch=(.*)$|) {
            $remote_url = $1;
            $remote_url =~ s|/$||;
            $remotes{$remote_name} = $remote_url;
        } elsif (m|^/manifest/project/\@path=(.*)$|) {
            die "Duplicate path!" if $path;
            $path = $1;
        } elsif (m|^/manifest/project/\@revision=(.*)$|) {
            die "Duplicate revision!" if $revision;
            $revision = $1;
        } elsif (m|^/manifest/project/\@name=(.*)$|) {
            die "Duplicate name!" if $name;
            $name = $1;
        } elsif (m|^/manifest/project/\@remote=(.*)$|) {
            die "Duplicate remote!" if $remote;
            $remote = $1;
        } elsif (m|^/manifest/project$|) {
            $path = $name if !$path && $name;
            $remote = $default_remote if !$remote && $default_remote;
            die "Missing path or revision!" if !$path || !$revision;
            if (!is_hash($revision)) {
                # FIXME: We can't do this if it's a tag
                # $revision = "$remote/$revision";
            }
            if ($remotes{$remote}) {
                our $url = "$remotes{$remote}/$name";
            } else {
                die "Could not find url for remote [$remote] default_remote [$default_remote]";
            }
            $hash->{$path} = "$remote $url $revision";
            ($path, $revision, $name, $remote) = (undef, undef, undef, undef);
        } elsif (m|^/manifest/default/\@remote=(.*)$|) {
            $default_remote = $1;
        }
    
    }
    close(FILE) or warn $! ? "Error closing pipe: $!" : "Exit status $? from sort";
    run("cd .repo/manifests && git checkout -");
    return $hash;
}

sub get_paths {
    my $paths;
    for my $hash (@_) {
        for my $key (keys %$hash) {
            $paths->{$key} = 1;
        }
    }
    return sort keys %$paths;
}

sub is_hash {
    my ($ref) = @_;
    return $ref =~ /^[a-z0-9]{40}$/;
}
